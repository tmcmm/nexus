# Copyright (c) 2016-present Sonatype, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM nexus.grupocgd.com:8444/cgd/registry.access.redhat.com/ubi8/ubi


ENV http_proxy=http://squid.grupocgd.com:3128
ENV https_proxy=http://squid.grupocgd.com:3128

ARG user=nexus
ARG group=nexus
ARG uid=200
ARG gid=200

# Create user & group
RUN groupadd --gid ${gid} ${group}
RUN useradd -ms /bin/bash -g ${gid} -u ${uid} ${user}

# Certificate
COPY ca-cgd.crt /etc/pki/ca-trust/source/anchors/ca-cgd.crt
RUN chmod 644 /etc/pki/ca-trust/source/anchors/ca-cgd.crt && update-ca-trust extract

LABEL vendor=Sonatype \
      maintainer="Sonatype <cloud-ops@sonatype.com>" \
      com.sonatype.license="Apache License, Version 2.0" \
      com.sonatype.name="Nexus Repository Manager base image"

#ARG NEXUS_VERSION=3.21.1-01
#ARG NEXUS_VERSION=3.21.2-03
ARG NEXUS_VERSION=3.22.0-02
ARG NEXUS_DOWNLOAD_URL=https://download.sonatype.com/nexus/3/nexus-${NEXUS_VERSION}-unix.tar.gz
#ARG NEXUS_DOWNLOAD_SHA256_HASH=aa5396eea6e619c32644a25a0225e55d43d8dc1e3567b7042a384a721d56332b -> HASH KEY 3.21.1
#ARG NEXUS_DOWNLOAD_SHA256_HASH=06d7fee9a919e481f08b7fa584c4a4680e84f4393cc8a21902a4c7e523c9f699 -> HASH KEY 3.21.2
ARG NEXUS_DOWNLOAD_SHA256_HASH=9fef4ee8d7423cd73d2bf3576acc22f8d1b76c9b0ff733e2d36f00b59f1b8388

RUN mkdir ${SONATYPE_DIR}/nexus
RUN mkdir /nexus-data
RUN mkdir ${SONATYPE_DIR}/sonatype-work

# Configure nexus runtime
ENV SONATYPE_DEPLOY_DIR=/opt/sonatype/nexus/deploy
ENV SONATYPE_DIR=/opt/sonatype
ENV NEXUS_HOME=${SONATYPE_DIR}/nexus \
    NEXUS_DATA=/nexus-data \
    NEXUS_CONTEXT='' \
    SONATYPE_WORK=${SONATYPE_DIR}/sonatype-work \
    DOCKER_TYPE='rh-docker'

ARG NEXUS_REPOSITORY_MANAGER_COOKBOOK_VERSION="release-0.5.20190212-155606.d1afdfe"
ARG NEXUS_REPOSITORY_MANAGER_COOKBOOK_URL="https://github.com/sonatype/chef-nexus-repository-manager/releases/download/${NEXUS_REPOSITORY_MANAGER_COOKBOOK_VERSION}/chef-nexus-repository-manager.tar.gz"

ADD solo.json.erb /var/chef/solo.json.erb

# Install using chef-solo
# Chef version locked to avoid needing to accept the EULA on behalf of whomever builds the image
RUN yum install -y --disableplugin=subscription-manager hostname procps \
    && curl -L https://www.getchef.com/chef/install.sh | bash -s -- -v 14.12.9 \
    && /opt/chef/embedded/bin/erb /var/chef/solo.json.erb > /var/chef/solo.json \
    && chef-solo \
       --recipe-url ${NEXUS_REPOSITORY_MANAGER_COOKBOOK_URL} \
       --json-attributes /var/chef/solo.json \
    && rpm -qa *chef* | xargs rpm -e \
    && rm -rf /etc/chef \
    && rm -rf /opt/chefdk \
    && rm -rf /var/cache/yum \
    && rm -rf /var/chef \
    && yum clean all


EXPOSE 8081
EXPOSE 8084
EXPOSE 8086
COPY nexus-repository-composer-0.0.4-bundle.kar ${SONATYPE_DEPLOY_DIR} 
# Plugin Keycloack https://github.com/flytreeleft/nexus3-keycloak-plugin
COPY nexus3-keycloak-plugin-0.4.0-bundle.kar ${SONATYPE_DEPLOY_DIR} 
COPY nexus-default.properties ${SONATYPE_DIR}/etc/nexus-default.properties
COPY keycloak.json ${SONATYPE_DIR}/nexus/etc/keycloak.json


ENV INSTALL4J_ADD_VM_PARAMS="-Xms3072m -Xmx3072m -XX:MaxDirectMemorySize=2g -Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs"
CMD ["sh", "-c", "${SONATYPE_DIR}/start-nexus-repository-manager.sh"]

RUN chown -R nexus:nexus ${SONATYPE_DIR}
RUN chown -R nexus:nexus ${NEXUS_DATA}

WORKDIR ${SONATYPE_DIR}