SERVER := nexus.grupocgd.com:8444
NAME   := ${SERVER}/cgd/nexus
MAJOR_VERSION := $$(cat VERSION | cut -f1 -d.)
MINOR_VERSION := $$(cat VERSION | cut -f2 -d.)
MICRO_VERSION := $$(cat VERSION | cut -f3 -d.)

TAG    := $$(cat VERSION)
IMG    := ${NAME}:${MAJOR_VERSION}.${MINOR_VERSION}.${MICRO_VERSION}
STABLE := ${NAME}:${MAJOR_VERSION}.${MINOR_VERSION}
LATEST := ${NAME}:latest

.PHONY: build
build:
	@ echo "... Building jenkins base image"
	@docker build --no-cache --rm --build-arg http_proxy=http://squid.grupocgd.com --build-arg https_proxy=http://squid.grupocgd.com -t ${IMG} . 
	@docker tag ${IMG} ${LATEST}
	@docker tag ${IMG} ${STABLE}
push:
	@docker push ${NAME}
 
login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS} ${SERVER}	

.PHONY: run
run: 
	@ echo "... Running nexus ${IMG} container"
	docker run -d --name nexus --restart always -p 8084:8084 -u 200:200 -p 8081:8081 -p 8086:8086 -it -v /deploy/nexus-dev/nexus-data/:/nexus-data ${IMG}
