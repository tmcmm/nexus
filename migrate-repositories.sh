#!/bin/bash

#################################################
## Objetivo : migrar repositorio Nexus para 3x
## Autor : Alexandre Guedes Breda
## Equipa: Canais
## Data : 19/10/2018
#################################################

## Variaveis
REPOSITORY="Agile_Snapshots_DEV"
EXTENSIONS="*.jar *.war *.pom *.xml *.md5 *.sha1 *.zip *.json *.txt *.md *.XML *.TXT *.ZIP *.JAR *.WAR *.POM *.XML *.MD5 *.SHA1 *.JSON"
AUTH="admin:admin123"
NEXUS_URL="http://10.11.34.45:8081"
set -x

## For deve ser realizado a partir da diretoria storage
for tosearch in $EXTENSIONS;
 do
       for file in `find $REPOSITORY -name $tosearch`;
       do
           length=${#file}
           path=${file:0:$length}
           echo "uploading path $path to $REPOSITORY"
           curl -kv -u $AUTH -X PUT --upload-file $path $NEXUS_URL/repository/$path -H 'authorization: Basic YWRtaW46YWRtaW4xMjM=' -H 'cache-control: no-cache' -H 'content-type: text/plain'
       done;
 done;
